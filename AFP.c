#define pr_fmt(fmt) "AFP: " fmt


#include <linux/init.h>
#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/kprobes.h>
#include <linux/sched.h>
#include <linux/cred.h>


MODULE_AUTHOR("Matvey Matvienko Sergeevich <matt.7d0@gmail.com>");
MODULE_DESCRIPTION("Advanced File Permissions module");
MODULE_LICENSE("GPL");


#define UIDRULE_STRING_SIZE 100
static char *uidRules_array[UIDRULE_STRING_SIZE];
static unsigned int uidRules_number = 0;
module_param_array(uidRules_array, charp, &uidRules_number, 0644);
MODULE_PARM_DESC(uidRules_array, "\"<uid>|<type of blockage>\"");

#define FILERULE_STRING_SIZE 1000
static char *fileRules_array[FILERULE_STRING_SIZE];
static unsigned int fileRules_number = 0;
module_param_array(fileRules_array, charp, &fileRules_number, 0644);
MODULE_PARM_DESC(fileRules_array, "\"<uid>|<function>|<file path>|<is_granted>\"");


#define FUNCTION_STRING_SIZE 255
#define FILENAME_STRING_SIZE 255
struct file_rule
{
	char function[FUNCTION_STRING_SIZE];			// string with function name
	char filename[FILENAME_STRING_SIZE];			// string with file path
	unsigned int is_granted;						// 0 - not granted, 1 - granted
};

struct uid_rule 
{
	unsigned int uid;			// user id
	unsigned int type;			// 0 - whitelist, 1 - blacklist
	struct file_rule *rules;	// rules to perform
	size_t rules_size;
};

struct uid_rule *uidRules = NULL;
size_t uidRules_size = 0;

unsigned int fileRules_to_uidRule_counter(unsigned int uidToCount)
{
	unsigned int i;
	unsigned int cnt = 0;
	for (i = 0; i < fileRules_number; i++)
	{
		char fileRule_copy[FILERULE_STRING_SIZE];
		strncpy(fileRule_copy, fileRules_array[i], FILERULE_STRING_SIZE-1);
		fileRule_copy[FILERULE_STRING_SIZE-1] = '\0';

		char *strsep_tmp = fileRule_copy;

		char *uid = strsep(&strsep_tmp, "|");

		long kstrtol_tmp;
		kstrtol(uid, 10, &kstrtol_tmp);
		unsigned int uid_number = (unsigned int)kstrtol_tmp;

		if (uid_number == uidToCount)
			cnt++;
	}

	return cnt;
}

int isUnique_uidRule(void)
{
	unsigned int i;
	for (i = 0; i < uidRules_number; i++)
	{
		char uidRule_copy_i[UIDRULE_STRING_SIZE];
		strncpy(uidRule_copy_i, uidRules_array[i], UIDRULE_STRING_SIZE-1);
		uidRule_copy_i[UIDRULE_STRING_SIZE-1] = '\0';

		char *strsep_tmp_i = uidRule_copy_i;

		char *uid_i = strsep(&strsep_tmp_i, "|");

		long kstrtol_tmp_i;
		kstrtol(uid_i, 10, &kstrtol_tmp_i);
		unsigned int uid_number_i = (unsigned int)kstrtol_tmp_i;

		unsigned int j;
		for (j = i + 1; j < uidRules_number; j++)
		{
			char uidRule_copy_j[UIDRULE_STRING_SIZE];
			strncpy(uidRule_copy_j, uidRules_array[j], UIDRULE_STRING_SIZE-1);
			uidRule_copy_j[UIDRULE_STRING_SIZE-1] = '\0';

			char *strsep_tmp_j = uidRule_copy_j;

			char *uid_j = strsep(&strsep_tmp_j, "|");

			long kstrtol_tmp_j;
			kstrtol(uid_j, 10, &kstrtol_tmp_j);
			unsigned int uid_number_j = (unsigned int)kstrtol_tmp_j;

			if (uid_number_i == uid_number_j)
				return 0;
		}
	}
	return 1;
}

int isUnique_fileRule(void)
{
	unsigned int i;
	for (i = 0; i < fileRules_number; i++)
	{
		char fileRule_copy_i[FILERULE_STRING_SIZE];
		strncpy(fileRule_copy_i, fileRules_array[i], FILERULE_STRING_SIZE-1);
		fileRule_copy_i[strlen(fileRule_copy_i)-2] = '\0';

		unsigned int j;
		for (j = i + 1; j < fileRules_number; j++)
		{
			char fileRule_copy_j[FILERULE_STRING_SIZE];
			strncpy(fileRule_copy_j, fileRules_array[j], FILERULE_STRING_SIZE-1);
			fileRule_copy_j[strlen(fileRule_copy_j)-2] = '\0';

			if (!strcmp(fileRule_copy_i, fileRule_copy_j))
				return 0;
		}
	}
	return 1;
}

unsigned int init_rules(void)
{
	if (!isUnique_uidRule() || !isUnique_fileRule())
		return 0;
		
	uidRules_size = (size_t)uidRules_number;
	uidRules = (struct uid_rule*)kmalloc(uidRules_size * sizeof(struct uid_rule), GFP_KERNEL);
	if (!uidRules)
		return 0;	
	
	unsigned int i = 0;
	for (i = 0; i < uidRules_number; i++)
	{
		char uidRule_copy[UIDRULE_STRING_SIZE];
		strncpy(uidRule_copy, uidRules_array[i], UIDRULE_STRING_SIZE-1);
		uidRule_copy[UIDRULE_STRING_SIZE-1] = '\0';

		char *strsep_tmp_i = uidRule_copy;

		char *uid_i = strsep(&strsep_tmp_i, "|");
		char *type_i = strsep(&strsep_tmp_i, "|");

		long kstrtol_tmp_i;
		kstrtol(uid_i, 10, &kstrtol_tmp_i);
		unsigned int uid_number_i = (unsigned int)kstrtol_tmp_i;
		kstrtol(type_i, 10, &kstrtol_tmp_i);
		unsigned int type_number_i = (unsigned int)kstrtol_tmp_i;

		uidRules[i].uid = uid_number_i;
		uidRules[i].type = type_number_i;
		uidRules[i].rules = NULL;
		uidRules[i].rules_size = (size_t)fileRules_to_uidRule_counter(uid_number_i);

		uidRules[i].rules = (struct file_rule*)kmalloc(uidRules[i].rules_size * sizeof(struct file_rule), GFP_KERNEL);
		if (!uidRules[i].rules)
		{
			unsigned int rev_i;
			if (i >= 1)
			{
				for (rev_i = i-1; rev_i > 0; rev_i--)
				{
					kfree(uidRules[rev_i].rules);
				}
				kfree(uidRules[0].rules);
			}
			kfree(uidRules);
			return 0;
		}

		unsigned int j = 0;
		unsigned int file_rule_indx = 0;
		for (j = 0; j < fileRules_number; j++)
		{
			char fileRule_copy[FILERULE_STRING_SIZE];
			strncpy(fileRule_copy, fileRules_array[j], FILERULE_STRING_SIZE-1);
			fileRule_copy[FILERULE_STRING_SIZE-1] = '\0';

			char *strsep_tmp_j = fileRule_copy;

			char *uid_j = strsep(&strsep_tmp_j, "|");
			char *function_j = strsep(&strsep_tmp_j, "|");
			char *filename_j = strsep(&strsep_tmp_j, "|");
			char *is_granted_j = strsep(&strsep_tmp_j, "|");

			long kstrtol_tmp_j;
			kstrtol(uid_j, 10, &kstrtol_tmp_j);
			unsigned int uid_number_j = (unsigned int)kstrtol_tmp_j;
			kstrtol(is_granted_j, 10, &kstrtol_tmp_j);
			unsigned int is_granted_number_j = (unsigned int)kstrtol_tmp_j;

			if (uid_number_i == uid_number_j)
			{
				strncpy(uidRules[i].rules[file_rule_indx].function, function_j, FUNCTION_STRING_SIZE);
				uidRules[i].rules[file_rule_indx].function[FUNCTION_STRING_SIZE] = '\0';

				strncpy(uidRules[i].rules[file_rule_indx].filename, filename_j, FILENAME_STRING_SIZE);
				uidRules[i].rules[file_rule_indx].filename[FILENAME_STRING_SIZE] = '\0';

				uidRules[i].rules[file_rule_indx].is_granted = is_granted_number_j;

				file_rule_indx++;
			}
		}
		
	}

	return i;
}

int remove_rules(void)
{
	size_t i;
	for (i = 0; i < uidRules_size; i++)
	{
		kfree(uidRules[i].rules);
	}
	kfree(uidRules);

	return i;
}

int isAllowed(int uid, const char *function, const char *filename)
{
	size_t finduid_i = 0;
	for (finduid_i = 0; finduid_i < uidRules_size; finduid_i++)
	{
		if (uidRules[finduid_i].uid == uid)
			break;
	}
	if (finduid_i == uidRules_size)
		return 1;

	size_t i = 0;
	for (i = 0; i < uidRules[finduid_i].rules_size; i++)
	{
		if (!strcmp(uidRules[finduid_i].rules[i].function, function)
		 && !strcmp(uidRules[finduid_i].rules[i].filename, filename))
		{
			return uidRules[finduid_i].rules[i].is_granted;
		}
	}
	if (i == uidRules[finduid_i].rules_size)
	{
		return uidRules[finduid_i].type;
	}
	return 1;
}


#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,7,0)
static unsigned long lookup_name(const char *name)
{
	struct kprobe kp = {
		.symbol_name = name
	};
	unsigned long retval;

	if (register_kprobe(&kp) < 0) return 0;
	retval = (unsigned long) kp.addr;
	unregister_kprobe(&kp);
	return retval;
}
#else
static unsigned long lookup_name(const char *name)
{
	return kallsyms_lookup_name(name);
}
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,11,0)
#define FTRACE_OPS_FL_RECURSION FTRACE_OPS_FL_RECURSION_SAFE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,11,0)
#define ftrace_regs pt_regs

static __always_inline struct pt_regs *ftrace_get_regs(struct ftrace_regs *fregs)
{
	return fregs;
}
#endif

#define USE_FENTRY_OFFSET 0

struct ftrace_hook 
{
	const char *name;
	void *function;
	void *original;

	unsigned long address;
	struct ftrace_ops ops;
};

static int resolve_hook_address(struct ftrace_hook *hook)
{
	hook->address = lookup_name(hook->name);

	if (!hook->address) {
		pr_debug("unresolved symbol: %s\n", hook->name);
		return -ENOENT;
	}

#if USE_FENTRY_OFFSET
	*((unsigned long*) hook->original) = hook->address + MCOUNT_INSN_SIZE;
#else
	*((unsigned long*) hook->original) = hook->address;
#endif

	return 0;
}

static void notrace ftrace_thunk(unsigned long ip, unsigned long parent_ip,
		struct ftrace_ops *ops, struct ftrace_regs *fregs)
{
	struct pt_regs *regs = ftrace_get_regs(fregs);
	struct ftrace_hook *hook = container_of(ops, struct ftrace_hook, ops);

#if USE_FENTRY_OFFSET
	regs->ip = (unsigned long)hook->function;
#else
	if (!within_module(parent_ip, THIS_MODULE))
		regs->ip = (unsigned long)hook->function;
#endif
}

int install_hook(struct ftrace_hook *hook)
{
	int err;

	err = resolve_hook_address(hook);
	if (err)
		return err;

	hook->ops.func = ftrace_thunk;
	hook->ops.flags = FTRACE_OPS_FL_SAVE_REGS
	                | FTRACE_OPS_FL_RECURSION
	                | FTRACE_OPS_FL_IPMODIFY;

	err = ftrace_set_filter_ip(&hook->ops, hook->address, 0, 0);
	if (err) {
		pr_debug("ftrace_set_filter_ip() failed: %d\n", err);
		return err;
	}

	err = register_ftrace_function(&hook->ops);
	if (err) {
		pr_debug("register_ftrace_function() failed: %d\n", err);
		ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);
		return err;
	}

	return 0;
}

void remove_hook(struct ftrace_hook *hook)
{
	int err;

	err = unregister_ftrace_function(&hook->ops);
	if (err) {
		pr_debug("unregister_ftrace_function() failed: %d\n", err);
	}

	err = ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);
	if (err) {
		pr_debug("ftrace_set_filter_ip() failed: %d\n", err);
	}
}

int install_hooks(struct ftrace_hook *hooks, size_t count)
{
	int err;
	size_t i;

	for (i = 0; i < count; i++) {
		err = install_hook(&hooks[i]);
		if (err)
			goto error;
	}

	return 0;

error:
	while (i != 0) {
		remove_hook(&hooks[--i]);
	}

	return err;
}

void remove_hooks(struct ftrace_hook *hooks, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++)
		remove_hook(&hooks[i]);
}

#ifndef CONFIG_X86_64
#error Currently only x86_64 architecture is supported
#endif

#if defined(CONFIG_X86_64) && (LINUX_VERSION_CODE >= KERNEL_VERSION(4,17,0))
#define PTREGS_SYSCALL_STUBS 1
#endif

#if !USE_FENTRY_OFFSET
#pragma GCC optimize("-fno-optimize-sibling-calls")
#endif

static char *duplicate_filename(const char __user *filename)
{
	char *kernel_filename;

	kernel_filename = kmalloc(4096, GFP_KERNEL);
	if (!kernel_filename)
		return NULL;

	if (strncpy_from_user(kernel_filename, filename, 4096) < 0) {
		kfree(kernel_filename);
		return NULL;
	}

	return kernel_filename;
}


#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_open)(struct pt_regs *regs);

static asmlinkage long sys_open(struct pt_regs *regs)
{
	long ret = -1;
	
	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->di);

	if (isAllowed((unsigned int)current_uid().val, "open", kernel_filename))
		ret = real_sys_open(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_open)(const char __user *filename, int flags, umode_t mode);

static asmlinkage long sys_open(const char __user *filename, int flags, umode_t mode)
{
	long ret = -1;
	
	char *kernel_filename;
	kernel_filename = duplicate_filename(filename);
	
	if (isAllowed((unsigned int)current_uid().val, "open", kernel_filename))
		ret = real_sys_open(filename, flags, mode);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_openat)(struct pt_regs *regs);

static asmlinkage long sys_openat(struct pt_regs *regs)
{
	long ret = -1;
	
	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->si);

	if (isAllowed((unsigned int)current_uid().val, "openat", kernel_filename))
		ret = real_sys_openat(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_openat)(int dfd, const char __user *filename, int flags, umode_t mode);

static asmlinkage long sys_openat(int dfd, const char __user *filename, int flags, umode_t mode)
{
	long ret = -1;
	
	char *kernel_filename;
	kernel_filename = duplicate_filename(filename);
	
	if (isAllowed((unsigned int)current_uid().val, "openat", kernel_filename))
		ret = real_sys_openat(dfd, filename, flags, mode);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_rename)(struct pt_regs *regs);

static asmlinkage long sys_rename(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->di);

	if (isAllowed((unsigned int)current_uid().val, "rename", kernel_filename))
		ret = real_sys_rename(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_rename)(const char __user *oldname, const char __user *newname);

static asmlinkage long sys_rename(const char __user *oldname, const char __user *newname)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(oldname);

	if (isAllowed((unsigned int)current_uid().val, "rename", kernel_filename))
		ret = real_sys_rename(oldname, newname);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_creat)(struct pt_regs *regs);

static asmlinkage long sys_creat(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->di);

	if (isAllowed((unsigned int)current_uid().val, "creat", kernel_filename))
		ret = real_sys_creat(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_creat)(const char __user *pathname, umode_t mode);

static asmlinkage long sys_creat(const char __user *pathname, umode_t mode)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(pathname);

	if (isAllowed((unsigned int)current_uid().val, "creat", kernel_filename))
		ret = real_sys_creat(pathname, mode);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_unlink)(struct pt_regs *regs);

static asmlinkage long sys_unlink(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->di);

	if (isAllowed((unsigned int)current_uid().val, "unlink", kernel_filename))
		ret = real_sys_unlink(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_unlink)(const char __user *pathname);

static asmlinkage long sys_unlink(const char __user *pathname)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(pathname);

	if (isAllowed((unsigned int)current_uid().val, "unlink", kernel_filename))
		ret = real_sys_unlink(pathname);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_unlinkat)(struct pt_regs *regs);

static asmlinkage long sys_unlinkat(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->si);

	if (isAllowed((unsigned int)current_uid().val, "unlinkat", kernel_filename))
		ret = real_sys_unlinkat(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_unlinkat)(int dfd, const char __user *pathname, int flag);

static asmlinkage long sys_unlinkat(int dfd, const char __user *pathname, int flag)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(pathname);

	if (isAllowed((unsigned int)current_uid().val, "unlinkat", kernel_filename))
		ret = real_sys_unlinkat(dfd, pathname, flag);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_link)(struct pt_regs *regs);

static asmlinkage long sys_link(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->di);

	if (isAllowed((unsigned int)current_uid().val, "link", kernel_filename))
		ret = real_sys_link(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_link)(const char __user *oldname, const char __user *newname);

static asmlinkage long sys_link(const char __user *oldname, const char __user *newname)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(oldname);

	if (isAllowed((unsigned int)current_uid().val, "link", kernel_filename))
		ret = real_sys_link(oldname, newname);

	kfree(kernel_filename);

	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long (*real_sys_linkat)(struct pt_regs *regs);

static asmlinkage long sys_linkat(struct pt_regs *regs)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename((void*) regs->si);

	if (isAllowed((unsigned int)current_uid().val, "linkat", kernel_filename))
		ret = real_sys_link(regs);

	kfree(kernel_filename);

	return ret;
}
#else
static asmlinkage long (*real_sys_linkat)(int olddfd, const char __user *oldname, int newdfd, const char __user *newname, int flags);

static asmlinkage long sys_linkat(int olddfd, const char __user *oldname, int newdfd, const char __user *newname, int flags)
{
	long ret = -1;

	char *kernel_filename;
	kernel_filename = duplicate_filename(oldname);

	if (isAllowed((unsigned int)current_uid().val, "linkat", kernel_filename))
		ret = real_sys_linkat(olddfd, oldname, newdfd, newname, flags);

	kfree(kernel_filename);

	return ret;
}
#endif


#define HOOK(_name, _function, _original)	\
	{										\
		.name = (_name),					\
		.function = (_function),			\
		.original = (_original),			\
	}

static struct ftrace_hook file_hooks[] = {
	HOOK("__x64_sys_open", sys_open, &real_sys_open),
	HOOK("__x64_sys_openat",  sys_openat,  &real_sys_openat),
	HOOK("__x64_sys_rename",  sys_rename,  &real_sys_rename),
	HOOK("__x64_sys_creat", sys_creat, &real_sys_creat),
	HOOK("__x64_sys_unlink", sys_unlink, &real_sys_unlink),
	HOOK("__x64_sys_unlinkat", sys_unlinkat, &real_sys_unlinkat),
	HOOK("__x64_sys_link", sys_link, &real_sys_link),
	HOOK("__x64_sys_linkat", sys_linkat, &real_sys_linkat),
};


static int __init AFP_init(void)
{	
	// params
	int inited = init_rules();
	if (inited == 0)
		return -1;
	pr_info("inited uid rules: %d\n", inited);

	// hooks
	int err;
	err = install_hooks(file_hooks, ARRAY_SIZE(file_hooks));
	if (err)
		return err;
	pr_info("hooks installed\n");

	pr_info("module loaded\n");

	return 0;
}

static void __exit AFP_exit(void)
{
	// hooks
	remove_hooks(file_hooks, ARRAY_SIZE(file_hooks));
	pr_info("hooks removed\n");

	// params
	int removed = remove_rules();
	pr_info("removed uid rules: %d\n", removed);

	pr_info("module unloaded\n");
}


module_init(AFP_init);
module_exit(AFP_exit);
