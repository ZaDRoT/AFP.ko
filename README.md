# AFP.ko

Advanced File Permissions - модуль ядра linux, перехватывающий системные вызовы для работы с файлами с проверкой доступа для пользователей.

Проект основан на: https://habr.com/ru/post/413241/

Перехватываемые функции:
- open
- openat
- rename
- creat
- unlink
- unlinkat
- link
- linkat

Синтаксис параметров модуля:
uidRules_array="\<uid\>|\<0 - whitelist, 1 - blacklist\>"[,...] fileRules_array="\<uid\>|\<function\>|\<file path\>|\<0 - not granted, 1 - granted\>"[,...]
